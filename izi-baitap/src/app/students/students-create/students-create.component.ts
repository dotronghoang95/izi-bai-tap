import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Schools } from 'src/app/model/schools.model';
import { Students } from 'src/app/model/students.model';
import { SchoolsService } from 'src/app/service/schools.service';
import { StudentsService } from 'src/app/service/students.service';

@Component({
  selector: 'app-students-create',
  templateUrl: './students-create.component.html',
  styleUrls: ['./students-create.component.scss']
})
export class StudentsCreateComponent implements OnInit {

  studentForm: FormGroup;
  schoolList: Schools[] = [];

  input = {
    id: '',
    name: '',
    age: '',
    gender: '',
    school: {},
  };

  constructor(private schoolService: SchoolsService,
    private studentService: StudentsService,
    private toastrService: ToastrService,
    private router: Router,
    private formBuilder: FormBuilder
  ) {

    this.studentForm = this.formBuilder.group({
      id: [null, []],
      name: [null, []],
      age: [null, []],
      gender: [null, []],
      school: [null, []],
    });
  }

  ngOnInit(): void {
    this.getSchool();
  }

  getSchool() {
    this.schoolService.get().subscribe(data => {
      this.schoolList = data;
    })
  }

  submit() {
    this.studentService.save(this.input).subscribe(() => {
      this.router.navigateByUrl('/student');
      this.toastrService.success('Thêm mới thành công', 'Thông báo');
    });
  };

}
