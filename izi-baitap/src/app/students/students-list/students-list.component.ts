import { Component, OnInit } from '@angular/core';
import { Students } from 'src/app/model/students.model';
import { StudentsService } from 'src/app/service/students.service';

@Component({
  selector: 'app-students-list',
  templateUrl: './students-list.component.html',
  styleUrls: ['./students-list.component.scss']
})

export class StudentsListComponent implements OnInit {
  studentsList: Students[] = [];
  page = 0;
  idDelete: any;

  constructor(private studentService: StudentsService) { }

  ngOnInit(): void {
    this.studentService.get().subscribe(data => {
      this.studentsList = data;
      console.log(data);
    })
  }

  showDelete(id: any) {
    this.idDelete = id;
  }

  delete() {
    this.studentService.delete(this.idDelete).subscribe(() => {
      this.ngOnInit();
    });
  }

}
