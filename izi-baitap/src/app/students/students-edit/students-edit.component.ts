import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Schools } from 'src/app/model/schools.model';
import { SchoolsService } from 'src/app/service/schools.service';
import { StudentsService } from 'src/app/service/students.service';

@Component({
  selector: 'app-students-edit',
  templateUrl: './students-edit.component.html',
  styleUrls: ['./students-edit.component.scss']
})
export class StudentsEditComponent implements OnInit {

  studentForm: FormGroup;
  id: any
  schoolList: Schools[] = [];

  input = {
    id: '',
    name: '',
    age: '',
    gender: '',
    school: {},
  };

  constructor(private studentService: StudentsService,
    private schoolService: SchoolsService,
    private route: ActivatedRoute,
    private toastrService: ToastrService,
    private router: Router,
    private formBuilder: FormBuilder,) {
    this.studentForm = this.formBuilder.group({
      id: [null, []],
      name: [null, []],
      age: [null, []],
      gender: [null, []],
      school: [null, []],
    });

  }

  ngOnInit(): void {
    this.id = this.route.snapshot.paramMap.get('id');
    console.log('zz', this.id);

    this.getStudent(this.id);

    this.getSchool();
  }

  getSchool() {
    this.schoolService.get().subscribe(data1 => {
      this.schoolList = data1;
    })
  }

  getStudent(id: number) {
    this.studentService.find(id).subscribe(data => {
      this.input = {
        id: data.id,
        name: data.name,
        age: data.age,
        gender: data.gender,
        school: data.school,
      };
    })
  }

  edit() {
    this.schoolService.update(this.id, this.input).subscribe(() => {
      this.router.navigate(['/student']);
      this.toastrService.success('Cập nhật thành công', 'Thông báo', {
        positionClass: 'toast-bottom-right'
      })
    })
  }

}
