import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { StudentsCreateComponent } from './students-create/students-create.component';
import { StudentsEditComponent } from './students-edit/students-edit.component';
import { StudentsListComponent } from './students-list/students-list.component';

const routes: Routes = [
  { path: '', component: StudentsListComponent },
  { path: 'add', component: StudentsCreateComponent },
  { path: 'edit/:id', component: StudentsEditComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StudentsRoutingModule { }
