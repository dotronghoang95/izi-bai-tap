import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { ClassName } from 'src/app/model/className.model';
import { ClassNameService } from 'src/app/service/class-name.service';
import { SchoolsService } from 'src/app/service/schools.service';

@Component({
  selector: 'app-schools-edit',
  templateUrl: './schools-edit.component.html',
  styleUrls: ['./schools-edit.component.scss']
})
export class SchoolsEditComponent implements OnInit {

  schoolForm: FormGroup;
  id: any
  classList: ClassName[] = [];

  input = {
    id: '',
    name: '',
    className: {
    },
  };

  constructor(private classService: ClassNameService,
    private schoolService: SchoolsService,
    private route: ActivatedRoute,
    private toastrService: ToastrService,
    private router: Router,
    private formBuilder: FormBuilder,) {
    this.schoolForm = this.formBuilder.group({
      id: [null, []],
      name: [null, []],
      className: [null, []],
    });

  }

  ngOnInit(): void {
    this.id = this.route.snapshot.paramMap.get('id');
    console.log('zz', this.id);

    this.getSchool(this.id);

    this.getClass();
  }

  getClass() {
    this.classService.get().subscribe(data1 => {
      this.classList = data1;
    })
  }

  getSchool(id: number) {
    this.schoolService.find(id).subscribe(data => {
      console.log('data', data.className.name);
      this.input = {
        id: data.id,
        name: data.name,
        className: data.className,
      };
    })
  }

  edit() {
    this.schoolService.update(this.id, this.input).subscribe(() => {
      this.router.navigate(['/school']);
      this.toastrService.success('Cập nhật thành công', 'Thông báo')
    })
  }
}
