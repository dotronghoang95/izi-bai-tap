import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SchoolsCreateComponent } from './schools-create/schools-create.component';
import { SchoolsEditComponent } from './schools-edit/schools-edit.component';
import { SchoolsListComponent } from './schools-list/schools-list.component';

const routes: Routes = [
  { path: '', component: SchoolsListComponent },
  { path: 'add', component: SchoolsCreateComponent },
  { path: 'edit/:id', component: SchoolsEditComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SchoolsRoutingModule { }
