import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { ClassName } from 'src/app/model/className.model';
import { ClassNameService } from 'src/app/service/class-name.service';
import { SchoolsService } from 'src/app/service/schools.service';

@Component({
  selector: 'app-schools-create',
  templateUrl: './schools-create.component.html',
  styleUrls: ['./schools-create.component.scss']
})
export class SchoolsCreateComponent implements OnInit {

  schoolForm: FormGroup;
  classList: ClassName[] = [];

  input = {
    id: '',
    name: '',
    className: {},
  };

  constructor(private schoolService: SchoolsService,
    private classNameService: ClassNameService,
    private toastrService: ToastrService,
    private router: Router,
    private formBuilder: FormBuilder
  ) {

    this.schoolForm = this.formBuilder.group({
      id: [null, []],
      name: [null, []],
      className: [null, []],
    });
  }

  ngOnInit(): void {
    this.getClassName();
  }

  getClassName() {
    this.classNameService.get().subscribe(data => {
      this.classList = data;
    })
  }

  submit() {
    this.schoolService.save(this.input).subscribe(() => {
      this.router.navigateByUrl('/school');
      this.toastrService.success('Thêm mới thành công', 'Thông báo');
    });
  };

}
