import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SchoolsRoutingModule } from './schools-routing.module';
import { SchoolsListComponent } from './schools-list/schools-list.component';
import { SchoolsCreateComponent } from './schools-create/schools-create.component';
import { SchoolsEditComponent } from './schools-edit/schools-edit.component';
import { ReactiveFormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';


@NgModule({
  declarations: [
    SchoolsListComponent,
    SchoolsCreateComponent,
    SchoolsEditComponent
  ],
  imports: [
    CommonModule,
    SchoolsRoutingModule,
    ReactiveFormsModule,
    NgxPaginationModule,
  ]
})
export class SchoolsModule { }
