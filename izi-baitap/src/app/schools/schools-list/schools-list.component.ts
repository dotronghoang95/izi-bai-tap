import { Component, OnInit } from '@angular/core';
import { Schools } from 'src/app/model/schools.model';
import { SchoolsService } from 'src/app/service/schools.service';

@Component({
  selector: 'app-schools-list',
  templateUrl: './schools-list.component.html',
  styleUrls: ['./schools-list.component.scss']
})
export class SchoolsListComponent implements OnInit {
  schoolList: Schools[] = [];
  page = 0;
  idDelete: any;

  constructor(private schoolsService: SchoolsService) { }

  ngOnInit(): void {
    this.schoolsService.get().subscribe(data => {
      this.schoolList = data;
    })
  }

  showDelete(id: any) {
    this.idDelete = id;
  }

  delete() {
    this.schoolsService.delete(this.idDelete).subscribe(() => {
      this.ngOnInit();
    });
  }

}
