import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'class',
    loadChildren: () => import('./class/class.module').then(module => module.ClassModule)
  },
  {
    path: 'school',
    loadChildren: () => import('./schools/schools.module').then(module => module.SchoolsModule)
  },
  {
    path: 'student',
    loadChildren: () => import('./students/students.module').then(module => module.StudentsModule)
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
