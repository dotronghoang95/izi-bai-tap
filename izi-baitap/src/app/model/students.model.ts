import { Schools } from "./schools.model";

export class Students {
    id?: number;
    name?: string;
    gender?: string;
    age?: string;
    school?: Schools;
}

