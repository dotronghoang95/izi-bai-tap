import { ClassName } from "./className.model";

export class Schools {
    id?: number;
    name?: string;
    className?: ClassName;
}