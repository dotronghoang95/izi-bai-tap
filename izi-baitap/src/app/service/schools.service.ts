import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
const API_URL = `${environment.apiUrl}`;

@Injectable({
  providedIn: 'root'
})
export class SchoolsService {

  constructor(private http: HttpClient) { }

  get(): Observable<any[]> {
    return this.http.get<any[]>(API_URL + '/schools');
  }

  save(school: any): Observable<any> {
    return this.http.post<any>(API_URL + '/schools', school);
  }

  find(id: any): Observable<any> {
    return this.http.get<any>(API_URL + '/schools/' + id);
  }

  update(id: any, data: any): Observable<any> {
    return this.http.put<any>(API_URL + '/schools/' + id, data);
  }

  delete(id: any): Observable<any> {
    return this.http.delete<any>(API_URL + '/schools/' + id);
  }
}
