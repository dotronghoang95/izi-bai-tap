import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

const API_URL = `${environment.apiUrl}`;
@Injectable({
  providedIn: 'root'
})
export class StudentsService {

  constructor(private http: HttpClient) { }

  get(): Observable<any[]> {
    return this.http.get<any[]>(API_URL + '/student');
  }

  save(student: any): Observable<any> {
    return this.http.post<any>(API_URL + '/student', student);
  }

  find(id: any): Observable<any> {
    return this.http.get<any>(API_URL + '/student/' + id);
  }

  update(id: any, data: any): Observable<any> {
    return this.http.put<any>(API_URL + '/student/' + id, data);
  }

  delete(id: any): Observable<any> {
    return this.http.delete<any>(API_URL + '/student/' + id);
  }
}

