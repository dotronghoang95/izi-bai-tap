import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { ClassName } from '../model/className.model';
const API_URL = `${environment.apiUrl}`;
@Injectable({
  providedIn: 'root'
})
export class ClassNameService {

  constructor(private http: HttpClient) { }

  get(): Observable<any[]> {
    return this.http.get<any[]>(API_URL + '/className');
  }

  save(className: any): Observable<any> {
    return this.http.post<any>(API_URL + '/className', className);
  }

  find(id: any): Observable<any> {
    return this.http.get<any>(API_URL + '/className/' + id);
  }

  update(id: any, data: any): Observable<any> {
    return this.http.put<any>(API_URL + '/className/' + id, data);
  }

  delete(id: any): Observable<any> {
    return this.http.delete<any>(API_URL + '/className/' + id);
  }
}
