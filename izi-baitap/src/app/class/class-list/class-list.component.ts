import { Component, OnInit } from '@angular/core';
import { ClassName } from 'src/app/model/className.model';
import { ClassNameService } from 'src/app/service/class-name.service';

@Component({
  selector: 'app-class-list',
  templateUrl: './class-list.component.html',
  styleUrls: ['./class-list.component.scss']
})
export class ClassListComponent implements OnInit {

  classList: ClassName[] = [];
  page = 0;
  idDelete: any;

  constructor(private classNameService: ClassNameService) { }

  ngOnInit(): void {
    this.classNameService.get().subscribe(data => {
      this.classList = data;
    })
  }

  showDelete(id: any) {
    this.idDelete = id;
  }

  delete() {
    this.classNameService.delete(this.idDelete).subscribe(() => {
      this.ngOnInit();
    });
  }

}
