import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ClassCreateComponent } from './class-create/class-create.component';
import { ClassEditComponent } from './class-edit/class-edit.component';
import { ClassListComponent } from './class-list/class-list.component';

const routes: Routes = [
  { path: '', component: ClassListComponent },
  { path: 'add', component: ClassCreateComponent },
  { path: 'edit/:id', component: ClassEditComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ClassRoutingModule { }
