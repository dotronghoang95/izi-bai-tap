import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { ActivatedRoute, ParamMap, Params, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { ClassNameService } from 'src/app/service/class-name.service';

@Component({
  selector: 'app-class-edit',
  templateUrl: './class-edit.component.html',
  styleUrls: ['./class-edit.component.scss']
})
export class ClassEditComponent implements OnInit {
  classForm: FormGroup;
  id: any

  input = {
    id: '',
    name: '',
  };

  constructor(private classService: ClassNameService,
    private route: ActivatedRoute,
    private toastrService: ToastrService,
    private router: Router,
    private formBuilder: FormBuilder,) {
    this.classForm = this.formBuilder.group({
      id: [null, []],
      name: [null, []],
    });

  }

  ngOnInit(): void {
    this.id = this.route.snapshot.paramMap.get('id');
    console.log('zz', this.id);

    this.getClass(this.id);
  }

  getClass(id: number) {
    this.classService.find(id).subscribe(data => {
      console.log('data', data);
      this.input = {
        id: data.id,
        name: data.name,
      };
    })
  }

  edit() {
    this.classService.update(this.id, this.input).subscribe(() => {
      this.router.navigate(['/class']);
      this.toastrService.success('Cập nhật thành công', 'Thông báo')
    })
  }
}
