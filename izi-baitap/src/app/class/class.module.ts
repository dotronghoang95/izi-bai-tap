import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ClassRoutingModule } from './class-routing.module';
import { ClassListComponent } from './class-list/class-list.component';
import { ClassCreateComponent } from './class-create/class-create.component';
import { ClassEditComponent } from './class-edit/class-edit.component';
import { ReactiveFormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';


@NgModule({
  declarations: [
    ClassListComponent,
    ClassCreateComponent,
    ClassEditComponent
  ],
  imports: [
    CommonModule,
    ClassRoutingModule,
    ReactiveFormsModule,
    NgxPaginationModule,
  ]
})
export class ClassModule { }
