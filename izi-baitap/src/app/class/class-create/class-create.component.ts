import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { ClassNameService } from 'src/app/service/class-name.service';

@Component({
  selector: 'app-class-create',
  templateUrl: './class-create.component.html',
  styleUrls: ['./class-create.component.scss']
})
export class ClassCreateComponent implements OnInit {

  classForm: FormGroup

  input = {
    id: '',
    name: '',
  };

  constructor(private classService: ClassNameService,
    private toastrService: ToastrService,
    private router: Router, private formBuilder: FormBuilder
  ) {

    this.classForm = this.formBuilder.group({
      id: [null, []],
      name: [null, []],
    });
  }

  ngOnInit(): void {
  }

  submit() {
    this.classService.save(this.input).subscribe(() => {
      this.router.navigateByUrl('/class');
      this.toastrService.success('Thêm mới thành công', 'Thông báo');
    });
  };

}

